package com.scy.btsmart.http.httphandler;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

import android.util.Log;

import com.scy.btsmart.common.HttpUtil;
import com.scy.btsmart.common.JsonUtil;
import com.scy.btsmart.http.httpmessage.HttpMessageReq;
import com.scy.btsmart.http.httpmessage.HttpMessageRsp;

public class RegisterCmdHandler implements HttpRequestHandler{

	private static final String TAG = "RegisterCmdHandler";
	
	public RegisterCmdHandler(){
	}
	
	public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException {
		// TODO Auto-generated method stub
		 String body = HttpUtil.getBodyFromRequest(request);
		 
		 //获取req对象
		 HttpMessageReq req = (HttpMessageReq)JsonUtil.json2Object(body, HttpMessageReq.class);
		 Log.i(TAG, "req:" + req.getmClientID()); 
		 
		 //构造返回值
		 HttpMessageRsp rsp = new HttpMessageRsp();
		 rsp.setmErrorCode(0);
		 rsp.setmErrorDes("Success");
		 
		 //构造rspjson
		 String rspjson = JsonUtil.object2Json(rsp);
		 
		 //返回结果
		 response.setStatusCode(HttpStatus.SC_OK);  
         StringEntity entity = new StringEntity(rspjson);  
         response.setEntity(entity);  
	}
}

package com.scy.btsmart.http.httphandler;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

import android.util.Log;

import com.scy.btsmart.bluetooth.BtSmartInterfaceManage;
import com.scy.btsmart.common.HttpUtil;
import com.scy.btsmart.common.JsonUtil;
import com.scy.btsmart.http.httpmessage.HttpQueryCharacteristicReq;
import com.scy.btsmart.http.httpmessage.HttpQueryCharacteristicRsp;

public class QueryCharacteristicHandler implements HttpRequestHandler {

	private static final String TAG = "QueryCharacteristicHandler";
	
	public QueryCharacteristicHandler(){
	}
	
	@Override
	public void handle(HttpRequest request, HttpResponse response, HttpContext context)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		 String body = HttpUtil.getBodyFromRequest(request);
		 
		 //获取req对象
		 HttpQueryCharacteristicReq req = (HttpQueryCharacteristicReq)JsonUtil.json2Object(body, HttpQueryCharacteristicReq.class);
		 HttpQueryCharacteristicRsp rsp = new HttpQueryCharacteristicRsp();
		 if(req == null){
			 rsp.setmErrorCode(-1);
			 rsp.setmErrorDes("fail");
		 }else{
			 Log.i(TAG, "req:" + req.getmClientID()); 
			 byte []value = BtSmartInterfaceManage.synGetCharacteristic(req.getmMac(), req.getmSUUID(), req.getmUUID());
			 if(value != null){
				 rsp.setmErrorCode(0);
				 rsp.setmErrorDes("Success");
				 rsp.setmValue(JsonUtil.encodeBinary(value));
			 }else{
				 rsp.setmErrorCode(-1);
				 rsp.setmErrorDes("fail");
			 }
		 }
		 
		 //构造rspjson
		String rspjson = JsonUtil.object2Json(rsp);
		 
		 //返回结果
		response.setStatusCode(HttpStatus.SC_OK);  
        StringEntity entity = new StringEntity(rspjson);  
        response.setEntity(entity);  
	}
}

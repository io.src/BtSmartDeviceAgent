package com.scy.btsmart.http.httphandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

import android.util.Log;

import com.scy.btsmart.bluetooth.BtSmartDevice;
import com.scy.btsmart.bluetooth.BtSmartInterfaceManage;
import com.scy.btsmart.common.HttpUtil;
import com.scy.btsmart.common.JsonUtil;
import com.scy.btsmart.http.httpmessage.HttpGetDeviceListRsp;
import com.scy.btsmart.http.httpmessage.HttpGetDeviceListRsp.DeviceItem;
import com.scy.btsmart.http.httpmessage.HttpMessageReq;

public class GetDeviceListHandler implements HttpRequestHandler{
	private static final String TAG = "GetDeviceListHandler";
	
	public GetDeviceListHandler(){
	}
	
	@Override
	public void handle(HttpRequest request, HttpResponse response, HttpContext context)
			           throws HttpException, IOException {
		// TODO Auto-generated method stub
		 String body = HttpUtil.getBodyFromRequest(request);
		 
		 //获取req对象
		 HttpMessageReq req = (HttpMessageReq)JsonUtil.json2Object(body, HttpMessageReq.class);
		 Log.i(TAG, "req:" + req.getmClientID()); 
		 
		 //构造返回消息
		 HttpGetDeviceListRsp rsp = new HttpGetDeviceListRsp();
		 rsp.setmErrorCode(0);
		 rsp.setmErrorDes("Success");
		 rsp.setmDeviceList(getDeviceList(rsp));
		 
		 //构造rspjson
		 String rspjson = JsonUtil.object2Json(rsp);
 
		 //返回结果
		 response.setStatusCode(HttpStatus.SC_OK);  
         StringEntity entity = new StringEntity(rspjson);  
         response.setEntity(entity);  
	}
	
	//获取设备列表
	private List<DeviceItem> getDeviceList(HttpGetDeviceListRsp rsp){
		List<BtSmartDevice>  devicelist = BtSmartInterfaceManage.getSmartDeviceList();
		if(devicelist == null){
			return null;
		}
		
		List<DeviceItem> mDeviceList = new ArrayList<DeviceItem>();
		for(int i =0; i < devicelist.size(); i++){
			DeviceItem tmp = rsp.new DeviceItem();
			BtSmartDevice tmpdevice = devicelist.get(i);
			tmp.setmName(tmpdevice.mDeviceName);
			tmp.setmMac(tmpdevice.mDeviceAddress);
			tmp.setmRssi(tmpdevice.mRssi);
			mDeviceList.add(tmp);
		}
		
		return mDeviceList;
	}

}

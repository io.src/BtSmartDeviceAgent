package com.scy.btsmart.http.httpserver;

import org.apache.http.protocol.HttpRequestHandlerRegistry;

import com.scy.btsmart.common.HttpUtil;
import com.scy.btsmart.http.httphandler.DeviceScanHandler;
import com.scy.btsmart.http.httphandler.GetDeviceListHandler;
import com.scy.btsmart.http.httphandler.GetDeviceStatusHandler;
import com.scy.btsmart.http.httphandler.QueryCharacteristicHandler;
import com.scy.btsmart.http.httphandler.QueryCharacteristicListHandler;
import com.scy.btsmart.http.httphandler.RegisterCmdHandler;
import com.scy.btsmart.http.httphandler.SetCharacteristicHandler;

public class HttpReqRegistManage {
	
	private static final String ALL_PATTERN = "*";
	private static HttpRequestHandlerRegistry mHttpReqRegist = null;
	
	public static HttpRequestHandlerRegistry getReqRegistHanler(){
		
		if(mHttpReqRegist == null){
			mHttpReqRegist = new HttpRequestHandlerRegistry();
		}
		
		//ע��handle
		RegisterCmdHandle();
		
		return mHttpReqRegist;
	}
	
	//ע��handle
	private static void RegisterCmdHandle(){
		mHttpReqRegist.register(ALL_PATTERN, new HomeCmdHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_REGISTER), new RegisterCmdHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_GETDEVICELIST), new GetDeviceListHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_GETDEVICESTATUS), new GetDeviceStatusHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_QUERY_CHARACTERISTIC_LIST), new QueryCharacteristicListHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_QUERY_CHARACTERISTIC), new QueryCharacteristicHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_SET_CHARACTERISTIC), new SetCharacteristicHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_DEVICESCAN), new DeviceScanHandler());
	}
}

/*   
 *   Copyright (C) 2012  Alvin Aditya H,
 *   					 Shanti F,
 *   					 Selviana 
 *   
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *       
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *       
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *    MA 02110-1301, USA.
 */

package com.scy.btsmart.http.httpserver;

import java.io.IOException;
import java.net.Socket;

import org.apache.http.HttpException;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpRequestHandlerRegistry;
import org.apache.http.protocol.HttpService;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseDate;
import org.apache.http.protocol.ResponseServer;

import android.content.Context;
import android.util.Log;

public class HttpThread extends Thread {
	
	private static final String TAG = "HttpThread";
	
	private Context mContext = null;
	
	public BasicHttpProcessor mHttproc;
	public HttpService mHttpServ = null;
	private BasicHttpContext mHttpContext = null;
	private HttpRequestHandlerRegistry mHttpReqRegist = null;
	
	Socket mSoket = null;
	
	public HttpThread(Context ctx, Socket soket, String threadName) {
		setContext(ctx);
		
		mSoket = soket;
		setName(threadName);
		mHttproc = new BasicHttpProcessor();
		mHttpContext = new BasicHttpContext();
		
		mHttproc.addInterceptor(new ResponseDate());
		mHttproc.addInterceptor(new ResponseServer());
		mHttproc.addInterceptor(new ResponseContent());
		mHttproc.addInterceptor(new ResponseConnControl());
	    
		mHttpServ = new HttpService(mHttproc, new DefaultConnectionReuseStrategy(), new DefaultHttpResponseFactory());
	    
		mHttpReqRegist = HttpReqRegistManage.getReqRegistHanler();
		
		mHttpServ.setHandlerResolver(mHttpReqRegist);	
	}

	public void run(){
		DefaultHttpServerConnection httpserver = new DefaultHttpServerConnection();
		try {
			httpserver.bind(this.mSoket, new BasicHttpParams());
			mHttpServ.handleRequest(httpserver, mHttpContext);
		} catch (IOException e) {
			Log.i(TAG, "Exception in HttpThread.java:can't bind. " + e.getMessage()); 
		} catch (HttpException e) {
			Log.i(TAG, "Exception in HttpThread.java:handle request. " + e.getMessage()); 
		} catch (Exception e){
			Log.i(TAG, "debug : error again. " + e.getMessage()); 
		}
		finally {
		
			try {
				httpserver.close();
			} catch (IOException e) {
				Log.i(TAG, "Excetion in HttpThread.java:can't shutdown. " + e.getMessage()); 
				e.printStackTrace();
			}
			 
		}
	}
	
	public void setContext(Context context) {
		this.mContext = context;
	}
	
	public Context getContext() {
		return mContext;
	}
}

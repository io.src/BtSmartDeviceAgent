package com.scy.btsmart.http.httpmessage;

public class HttpQueryCharacteristicReq extends HttpMessageReq{
    private String           mName;
    
    private String           mMac;
    private String           mSUUID;
    private String           mUUID;
   
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public String getmMac() {
		return mMac;
	}
	public void setmMac(String mMac) {
		this.mMac = mMac;
	}
	public String getmSUUID() {
		return mSUUID;
	}
	public void setmSUUID(String mSUUID) {
		this.mSUUID = mSUUID;
	}
	public String getmUUID() {
		return mUUID;
	}
	public void setmUUID(String mUUID) {
		this.mUUID = mUUID;
	}
    
    
}

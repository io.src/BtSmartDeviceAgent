package com.scy.btsmart.http.httpmessage;

public class HttpGetDeviceStatusRsp extends HttpMessageRsp{
   
	private  int     mRssi;

	public HttpGetDeviceStatusRsp(){
	}

	public int getmRssi() {
		return mRssi;
	}

	public void setmRssi(int mRssi) {
		this.mRssi = mRssi;
	}
}



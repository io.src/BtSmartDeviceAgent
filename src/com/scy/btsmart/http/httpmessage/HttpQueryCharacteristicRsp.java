package com.scy.btsmart.http.httpmessage;


public class HttpQueryCharacteristicRsp extends HttpMessageRsp{

    private String           mValue;

    public HttpQueryCharacteristicRsp(){
    	
    }

	public String getmValue() {
		return mValue;
	}

	public void setmValue(String mValue) {
		this.mValue = mValue;
	}
    
    
}

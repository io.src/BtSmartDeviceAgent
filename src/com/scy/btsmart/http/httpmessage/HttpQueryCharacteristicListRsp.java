package com.scy.btsmart.http.httpmessage;

import java.util.ArrayList;
import java.util.List;

public class HttpQueryCharacteristicListRsp extends HttpMessageRsp{
    
    private List<ServerItem> mServerList; 
    
    public class ServerItem{
    	private String    mServer;
    	private String    mSUUID;
    	private List<CharacteristicItem> mCharaList; 
    	
    	
        public class CharacteristicItem{
        	private String    mName;
        	private String    mUUID;
        	private String    mDes;
        	
        	public CharacteristicItem(){
        	}

			public String getmName() {
				return mName;
			}

			public void setmName(String mName) {
				this.mName = mName;
			}

			public String getmUUID() {
				return mUUID;
			}

			public void setmUUID(String mUUID) {
				this.mUUID = mUUID;
			}

			public String getmDes() {
				return mDes;
			}

			public void setmDes(String mDes) {
				this.mDes = mDes;
			}
        	
        	
        }

		public String getmServer() {
			return mServer;
		}


		public void setmServer(String mServer) {
			this.mServer = mServer;
		}


		public String getmSUUID() {
			return mSUUID;
		}


		public void setmSUUID(String mSUUID) {
			this.mSUUID = mSUUID;
		}


		public List<CharacteristicItem> getmCharaList() {
			return mCharaList;
		}


		public void setmCharaList(List<CharacteristicItem> mCharaList) {
			this.mCharaList = mCharaList;
			this.mCharaList = new ArrayList<CharacteristicItem>(mCharaList.size());  
	    	for(CharacteristicItem uuid : mCharaList){
	    		this.mCharaList.add(uuid);  
	    	}
		} 
    }

	public List<ServerItem> getmServerList() {
		return mServerList;
	}

	public void setmServerList(List<ServerItem> mServerList) {
		this.mServerList = new ArrayList<ServerItem>(mServerList.size());  
    	for(ServerItem uuid : mServerList){
    		this.mServerList.add(uuid);  
    	}
	}
}

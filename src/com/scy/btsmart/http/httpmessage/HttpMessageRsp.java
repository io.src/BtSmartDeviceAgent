package com.scy.btsmart.http.httpmessage;

public class HttpMessageRsp {

    private int mErrorCode;

    private String mErrorDes;
	
	public HttpMessageRsp(){
		mErrorCode = 0;
		mErrorDes  = "Success";
	}

	public int getmErrorCode() {
		return mErrorCode;
	}

	public void setmErrorCode(int mErrorCode) {
		this.mErrorCode = mErrorCode;
	}

	public String getmErrorDes() {
		return mErrorDes;
	}

	public void setmErrorDes(String mErrorDes) {
		this.mErrorDes = mErrorDes;
	}
}

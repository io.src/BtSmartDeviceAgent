package com.scy.btsmart.http.httpmessage;

public class HttpDeviceScanReq extends HttpMessageReq{
	
	private int mScanStatus;

	public HttpDeviceScanReq(){
	}

	public int getmScanStatus() {
		return mScanStatus;
	}

	public void setmScanStatus(int mScanStatus) {
		this.mScanStatus = mScanStatus;
	}
}

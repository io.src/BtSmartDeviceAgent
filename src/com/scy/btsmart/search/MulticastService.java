package com.scy.btsmart.search;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import com.scy.btsmart.common.BtSmartCommon;
import com.scy.btsmart.common.NetUtil;
import com.scy.btsmart.conf.ConfParamName;
import com.scy.btsmart.conf.ConfigParamManage;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class MulticastService extends Thread{
	
	private static final String TAG = "MulticastService";
	
	//多播socket
	private MulticastSocket    mMulticastSocket = null;
	
	//组播ip
	InetAddress               mGroupIp = null;
	
	//组播端口
	int                       mMulPort = 0;
	
	//数据回调handler
	private Handler            mHandler = null;
	
	private static boolean mIsExit = true;
	
	public MulticastService(MulticastSocket socket, InetAddress groupip, int mulport, Handler handler){
		mMulticastSocket = socket;
		mGroupIp         = groupip;
		mMulPort         = mulport;
		mHandler = handler;
	}

	//停止服务
	public void stopServer() {
		mIsExit = false;
	}
	
	//线程处理函数
	public void run(){
		if(mMulticastSocket == null){
			Log.i(TAG, "mMulticastSocket is null."); 
			return;
		}
		
		//设置服务状态
		mIsExit = true;
		
		byte []buf = new byte[100];
		DatagramPacket recv = new DatagramPacket(buf, buf.length);
		while(mIsExit){
			
			//接收数据
			try {
				    //接收数据
					mMulticastSocket.receive(recv);
		
					//处理数据
					onRecvData(recv.getData());
					
			} catch (IOException e) {
				Log.i(TAG, "mMulticastSocket.receive(recv). " + e.getMessage()); 
			}
		}
		
		try {
			mMulticastSocket.leaveGroup(mGroupIp);
			mMulticastSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "mMulticastSocket.leaveGroup(mGroupIp);. " + e.getMessage()); 
		}
	}
	
	//处理数据
	public void onRecvData(byte []buff){
		if(buff == null){
			return;
		}
		
		if(mHandler != null){
			MulticastDataNotify(buff);
		}
		
		//
		if(IsClientRequstData(buff)){
			//响应组播消息
			SendMulticastHostIp();
			
		}else if(IsClientResponseData(buff)){
			
			AddSmartAgent(buff);
		}else{
			Log.i(TAG, "void onRecvData(byte []buff). ");
		}
	}
	
	//发送响应数据
	public void sendMulticastData(byte []rsp){
		if(rsp == null || rsp.length <= 0){
			return;
		}
		
		//生成发送包
		DatagramPacket data= new DatagramPacket(rsp, rsp.length, mGroupIp, mMulPort);
		
		//发送数据
		try {
			mMulticastSocket.send(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "mMulticastSocket.send(data). " + e.getMessage());
		}
	}
	
	//发送组播数据通知
	public void MulticastDataNotify(byte []buff){
		Bundle messageBundle = new Bundle();
		Message msg = Message.obtain(mHandler, BtSmartCommon.MESSAGE_MULTICAST_DATA);
		messageBundle.putByteArray(BtSmartCommon.EXTRA_MULTICAST_DATA, buff);
		msg.setData(messageBundle);
		msg.sendToTarget();
	}
	
	//btmart 数据包校验
	public boolean IsClientRequstData(byte []buff){
		if(buff == null || buff.length < 3){
			return false;
		}
		
		if(buff[0] == BtSmartCommon.SCY[0] && buff[1] == BtSmartCommon.SCY[1] && buff[2] == BtSmartCommon.SCY[2]){
			return true;
		}
		
		return false;
	}
	
	//btmart 响应数据
	public boolean IsClientResponseData(byte []buff){
		if(buff == null || buff.length < 7){
			return false;
		}
		
		if(buff[0] == BtSmartCommon.BTAGENT[0] && buff[1] == BtSmartCommon.BTAGENT[1] && 
		   buff[2] == BtSmartCommon.BTAGENT[2] && buff[3] == BtSmartCommon.BTAGENT[3]&&
		   buff[4] == BtSmartCommon.BTAGENT[4] && buff[5] == BtSmartCommon.BTAGENT[5] &&
		   buff[6] == BtSmartCommon.BTAGENT[6]){
			return true;
		}
		
		return false;
	}
	
	//添加smartagent
	public void AddSmartAgent(byte []buff){
		String agentip = new String(buff);
		agentip = agentip.substring(6);
		
		//判断是否为本机ip
		if(!agentip.startsWith(NetUtil.getLocalHostIp())){
			MulticastServiceAgentSearch.addBtSmartAgent(agentip);
		}
	}
	
	//发送回应回应本机IP,以及http监听端口
	public void SendMulticastHostIp(){
		
		//本机IP
		String hostip = NetUtil.getLocalHostIp();
		
		//获取http监听端口
		int httpport = ConfigParamManage.getIntValue(ConfParamName.HTTP_PORT, 8080);
		
		String httpip = BtSmartCommon.BTAGENT + hostip + ":" +Integer.toString(httpport);
		
		sendMulticastData(httpip.getBytes());
	}
}

















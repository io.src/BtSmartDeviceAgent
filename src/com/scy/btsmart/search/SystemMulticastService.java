package com.scy.btsmart.search;

import com.scy.btsmart.common.BtSmartCommon;
import com.scy.btsmart.common.NetUtil;
import com.scy.btsmart.conf.ConfParamName;
import com.scy.btsmart.conf.ConfigParamManage;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

public class SystemMulticastService extends Service{

	private MulticastService  mMulService = null;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		String gropip = ConfigParamManage.getStringValue(ConfParamName.MULTICAST_IP, "239.255.255.250");
		int mulport = ConfigParamManage.getIntValue(ConfParamName.MULTICAST_PORT, 1901);
		
		 //开启组播
		 NetUtil.enableMulticast(this.getApplicationContext());
		 
		//创建服务
		mMulService = NetUtil.createMulticastService(gropip, mulport, mSearchHandler);
	}
	
	@Override
	public void onDestroy() {
		
		if(mMulService != null){
			mMulService.stopServer();
			mMulService.stop();
		}

		NetUtil.disenableMulticast();
		
		super.onDestroy();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {	
		//启动线程
		if(mMulService != null){
			mMulService.start();
		}
		
		return START_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	 private final static Handler mSearchHandler = new  Handler(){ 
		@Override
	    public void handleMessage(Message msg) {
           switch (msg.what) {                
           case BtSmartCommon.MESSAGE_MULTICAST_DATA: {
           	   Bundle tmpbunle = msg.getData();
           	   byte []agentipbyte = tmpbunle.getByteArray(BtSmartCommon.EXTRA_MULTICAST_DATA);
           	   String agentip = new String(agentipbyte);
           	   if(!agentip.isEmpty()){
           		   
           	   }
           }
         }
		}
	 };
	
}

package com.scy.btsmart.common;


import java.util.UUID;

public class BtSmartCommon {

	public static final byte  []SCY= {0x53, 0x43, 0x59};
	public static final byte  []BTAGENT= {0x42, 0x54, 0x41, 0x47, 0x45, 0x4e, 0x54};

	
	//设置通知属性uuid
	public static final UUID CCC = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
	
	//连接超时间
	public static final int CONNECT_TIME_OUT = 1;
	
	//消息超时时间
	public static final int MESSAGE_TIME_OUT = 3;
	
	//连接通知范围1~100
	public static final int MESSAGE_CONNECTED = 1;
	public static final int MESSAGE_DISCONNECTED = 2;
	
	//characteristic 101~200
	public static final int MESSAGE_CHARACTERISTIC_CHANGE = 101;
	public static final int MESSAGE_CHARACTERISTIC_VALUE = 102;
	public static final int MESSAGE_CHARACTERISTIC_READ = 103;
	public static final int MESSAGE_CHARACTERISTIC_WRITE = 104;
	
	//descriptor 201~300
	public static final int MESSAGE_DESCRIPTOR_VALUE = 201;
	public static final int MESSAGE_DESCRIPTOR_READ = 202;
	public static final int MESSAGE_DESCRIPTOR_WRITE = 203;
	
	//服务发现消息
	public static final int MESSAGE_SERVICE_DISCOVER = 301;
	
	//操作结果1001
	public static final int MESSAGE_WRITE_COMPLETE = 1001;
	public static final int MESSAGE_REQUEST_FAILED = 1002;
	public static final int MESSAGE_REQUEST_FINISH = 1003;
	
	//客户端搜索2001
	//组播数据广播
	public static final int MESSAGE_MULTICAST_DATA = 2001; 
	
	//组播数据
	public static final String EXTRA_MULTICAST_DATA  = "MulticastData";
	
	
	public static final String EXTRA_DEVICE_NAME  = "DEVICENAME";
	public static final String EXTRA_DEVICE_ADDR  = "DEVICEADDR";
	public static final String EXTRA_VALUE = "CVALUE";
	public static final String EXTRA_SERVICE_UUID = "SERVUUID";
	public static final String EXTRA_CHARACTERISTIC_UUID = "CHARUUID";
	public static final String EXTRA_DESCRIPTOR_UUID = "DESCUUID";
	public static final String EXTRA_REQUEST_ID = "REQUESTID";
	
	public static final String EXTRA_RESULT = "CResult";
	public static final String EXTRA_ErrDES = "ErrDes";	
	
	
	

}

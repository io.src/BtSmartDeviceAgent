package com.scy.btsmart.common;

import com.google.gson.Gson;

public class JsonUtil {
    /**
     *将java对象转json串
     * 
     * 
     * @return 
    */
	public static String object2Json(Object obj){
		//将java对象转换为json对象  
		Gson gson=new Gson();
        return gson.toJson(obj); 
	}
    /**
     * 通过jsonstr转换字符串
     * 
     * 
     * @return 
    */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object json2Object(String jsonstr, Class beanclass){
      //将json字符串转换为json对象  
		Gson gson=new Gson();
        return gson.fromJson(jsonstr, beanclass);
	}
    /**
     * 加密二级制
     * 
     * 
     * @return 
    */
	public static String encodeBinary(byte []binbuff){
		if(binbuff == null){
			return "";
		}
		
		final String BASE_UUID_FORMAT = "%x-";
		
		String encodestr = new String();
		for(int i = 0; i < binbuff.length; i++){
			encodestr += String.format(BASE_UUID_FORMAT, binbuff[i]);
		}
		
		return encodestr;
	}
    /**
     * 解密二进制
     * 
     * 
     * @return 
    */
	public static byte[] decodeBinary(String encodestr){
		if(encodestr == null || encodestr.isEmpty()){
			return null;
		}
		
		//分割字符
		String []binstr = encodestr.split("-");
		byte []tmpbuff = new byte[binstr.length];
		for(int j = 0; j < binstr.length; j++){
			Integer tmp = Integer.parseInt(binstr[j], 16);
			byte testtmp = tmp.byteValue();
			tmpbuff[j] = testtmp;
		}
		
		return tmpbuff;
	}
}

package com.scy.btsmart.bluetooth;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.scy.btsmart.conf.BtProfileConfManage;
import com.scy.btsmart.conf.BtSmartDeviceServerItem;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

public class BtSmartInterfaceManage {
	
	private static final String TAG = "BtSmartInterfaceManage";
	
    private static BtSmartScanService         mScanService = null;
    
    private static BtSmartMessageService      mSmartMessageService = null; 
    /**
     * 开启服务
     * 
     * 
     * @return 
    */
    public static void StartServer(Context context){
    	
		//加载配置文件属性
		BtProfileConfManage.loadBtProfile(context.getApplicationContext());
		
    	//开启消息处理服务
    	startSmartScanService(context);
    	
    	//开启消息处理服务
    	startSmartMessageService(context);
    }
    /**
     * 停止服务
     * 
     * 
     * @return 
    */
    public static void StopServer(Context context){
    	
    	//开启消息处理服务
    	stopSmartScanService(context);
    	
    	//开启消息处理服务
    	stopSmartMessageService(context);
    }
    /**
     * 解除绑定服务
     * 
     * 
     * @return 
    */    
    public static void unBindService(Context context){
    	
		//解bind扫描服务
    	context.unbindService(mScanServiceConnection);
		
		//解bind扫描服务
    	context.unbindService(mMessageServiceConnection);
    }
    /**
     * 获取设备列表
     * 
     * 
     * @return 
    */
    public static List<BtSmartDevice> getSmartDeviceList(){
    	return BtSmartDeviceManage.getSmartDeviceList();
    }
    /**
     * 开始扫描扫描设备
     * 
     * 
     * @return 
    */
    public static boolean startScanLeDevice(){
    	if(mScanService == null){
    		return false;
    	}
    	
    	mScanService.startScanLeDevice();
    	
    	return true;
    }
    /**
     * 停止扫描设备
     * 
     * 
     * @return 
    */
    public static boolean stopScanLeDevice(){
    	if(mScanService == null){
    		return false;
    	}
    	
    	mScanService.stopScanLeDevice();
    	
    	return true;
    }
    /**
     * 获取设备支持的属性
     * 
     * 
     * @return 
    */
    public static List<BtSmartDeviceServerItem> getDeviceCharacteristicList(String address){
    	BtSmartDevice device = BtSmartDeviceManage.getSmartDevice(address);
    	if(device == null){
    		return null;
    	}
    	
    	List<BtSmartDeviceServerItem> tmplist = new ArrayList<BtSmartDeviceServerItem>();
    	for(UUID suuid : device.mSUUIDList){
    		BtSmartDeviceServerItem deviceserver = BtProfileConfManage.getBtServerItem(suuid);
    		if(deviceserver != null){
    			tmplist.add(deviceserver);
    		}
    	}
    	
    	return tmplist;
    }
    /**
     * 取属性值
     * 
     * 
     * @return 
    */   
    public static int synGetRemoteRssi(String address){
    	if(mSmartMessageService == null){
    		Log.i(TAG, "mSmartMessageService or value is null"); 
    		return 0;
    	}
    	
    	BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
    	int reqid = BtSmartRequestIDManage.mallocRequestID();
    	int rssi =  mSmartMessageService.synGetRemoteRssi(reqid, device, 3000);
    	BtSmartRequestIDManage.recycleRequestID(reqid);
    	
    	return rssi;
    }
    /**
     * 取属性值
     * 
     * 
     * @return 
    */   
    public static byte[] synGetCharacteristic(String address, String service, String characteristic){
    	if(mSmartMessageService == null){
    		Log.i(TAG, "mSmartMessageService or value is null"); 
    		return null;
    	}
    	
    	BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
    	int reqid = BtSmartRequestIDManage.mallocRequestID();
    	 byte []value =  mSmartMessageService.synGetCharacteristic(reqid, device, UUID.fromString(service), 
    			                                                   UUID.fromString(characteristic), 3000);
    	BtSmartRequestIDManage.recycleRequestID(reqid);
    	return value;
    }
    /**
     * 设置属性
     * 
     * 
     * @return 
    */   
    public static boolean synSetCharacteristic(String address, String service, String characteristic, byte []value){
    	if(mSmartMessageService == null || value == null){
    		Log.i(TAG, "mSmartMessageService or value is null"); 
    		return false;
    	}
    	
    	BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
    	int reqid = BtSmartRequestIDManage.mallocRequestID();
    	mSmartMessageService.synSetCharacteristic(reqid, device, UUID.fromString(service), UUID.fromString(characteristic), 
                                                  value, 3000);
    	
    	BtSmartRequestIDManage.recycleRequestID(reqid);
    	return true;
    }
    
    //启动执行扫描服务
	private static void startSmartScanService(Context context){
		//启动服务
		Intent bindIntent = new Intent(context, BtSmartScanService.class); 
		context.bindService(bindIntent, mScanServiceConnection, Context.BIND_AUTO_CREATE);
    }
	
    //停止执行扫描服务
	private static void stopSmartScanService(Context context){
		
		//启动服务
		Intent bindIntent = new Intent(context, BtSmartScanService.class); 
		context.stopService(bindIntent);
    }
	
    //启动执行消息服务
	private static void startSmartMessageService(Context context){
		//启动服务
		Intent bindIntent = new Intent(context, BtSmartMessageService.class); 
		context.bindService(bindIntent, mMessageServiceConnection, Context.BIND_AUTO_CREATE);
    }
    //停止执行消息服务
	private static void stopSmartMessageService(Context context){
		
		//启动服务
		Intent bindIntent = new Intent(context, BtSmartMessageService.class); 
		context.stopService(bindIntent);
    }
    /**
     * Callbacks for changes to the state of the connection to BtSmartMessageService.
     */
    private static ServiceConnection mScanServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
        	mScanService = ((BtSmartScanService.LocalBinder) rawBinder).getService();
            if (mScanService == null) {
            	Log.i(TAG, "Connect BtSmartMessageService fail"); 
            }
        }

        public void onServiceDisconnected(ComponentName classname) {
        	mScanService = null;
        }
    };
    
    /**
     * Callbacks for changes to the state of the connection to BtSmartMessageService.
     */
    private static ServiceConnection mMessageServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
        	mSmartMessageService = ((BtSmartMessageService.LocalBinder) rawBinder).getService();
            if (mSmartMessageService == null) {
            	Log.i(TAG, "Connect BtSmartMessageService fail"); 
            }
        }

        public void onServiceDisconnected(ComponentName classname) {
        	mSmartMessageService = null;
        }
    };
}

package com.scy.btsmart.bluetooth;

import java.util.HashSet;
import java.util.Set;


public class BtSmartRequestIDManage {
	private static Integer mBaseRequestID = 1;
	private static Set<Integer> mFreeRequestIDSet = new HashSet<Integer>();
	
	public static int mallocRequestID(){
		int tmpid = 0;
		
		synchronized(mFreeRequestIDSet){
			if(!mFreeRequestIDSet.isEmpty()){
				tmpid = mFreeRequestIDSet.iterator().next().intValue();
			}else{
				tmpid = mBaseRequestID;
				mBaseRequestID++;
			}
		}
		
		return tmpid;
	}
	
	public static void recycleRequestID(int id){
		if(id <= 0){
			return;
		}
		
		synchronized(mFreeRequestIDSet){
			mFreeRequestIDSet.add(id);
		}
	}
	
}

package com.scy.btsmart.bluetooth;

import java.util.UUID;

import com.scy.btsmart.common.BtSmartCommon;

import android.bluetooth.BluetoothDevice;
import android.os.Handler;


public class BtSmartRequest {
	
	public enum RequestType {
		CHARACTERISTIC_NOTIFY, 
		READ_CHARACTERISTIC, 
		READ_DESCRIPTOR, 
		READ_RSSI, 
		WRITE_CHARACTERISTIC, 
		WRITE_DESCRIPTOR
	};
	
	public RequestType         m_Type;
	public int                 m_RequestId;
	public BluetoothDevice     m_SmartDevice;
	public UUID                m_ServiceUuid;
	public UUID                m_CharacteristicUuid;
	public UUID                m_DescriptorUuid;
	public Handler             m_NotifyHandler;
	public byte                []m_Value;
	public byte                []m_ResultValue;
	public int                 m_Rssi;
	public int                 m_ErrCode;
	public int                 m_TimeOut;
	public long                m_StartTime;
	public boolean             m_IsSynRequest;
	public boolean             m_IsFinish;
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = null;
		this.m_CharacteristicUuid = null;
		this.m_NotifyHandler = null;
		this.m_Value = null;
		if(timeout > 0){
			this.m_TimeOut = timeout;
		}else{
			this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = true;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, UUID service, 
                          UUID characteristic, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_NotifyHandler = null;
		this.m_Value = null;
		if(timeout > 0){
			this.m_TimeOut = timeout;
		}else{
			this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = true;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, UUID service, 
                          UUID characteristic, UUID descriptor, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_DescriptorUuid = descriptor;
		this.m_NotifyHandler = null;
		this.m_Value = null;
		if(timeout > 0){
			this.m_TimeOut = timeout;
		}else{
			this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = true;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, UUID service, 
            			  UUID characteristic, byte []value, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_DescriptorUuid = null;
		this.m_NotifyHandler = null;
		this.m_Value = value;
		this.m_Rssi = 0;
		if(timeout > 0){
		this.m_TimeOut = timeout;
		}else{
		this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = true;
		this.m_ResultValue = null;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, UUID service, 
                          UUID characteristic, UUID descriptor, byte []value, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_DescriptorUuid = descriptor;
		this.m_NotifyHandler = null;
		this.m_Value = value;
		if(timeout > 0){
			this.m_TimeOut = timeout;
		}else{
			this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = true;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, Handler handler, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = null;
		this.m_CharacteristicUuid = null;
		this.m_NotifyHandler = handler;
		this.m_Value = null;
		if(timeout > 0){
			this.m_TimeOut = timeout;
		}else{
			this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = false;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, UUID service, 
                          UUID characteristic, Handler handler, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_DescriptorUuid = null;
		this.m_NotifyHandler = handler;
		this.m_Value = null;
		if(timeout > 0){
		this.m_TimeOut = timeout;
		}else{
		this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = false;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId, BluetoothDevice device, UUID service, 
			              UUID characteristic, UUID descriptor, Handler handler, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_DescriptorUuid = descriptor;
		this.m_NotifyHandler = handler;
		this.m_Value = null;
		if(timeout > 0){
			this.m_TimeOut = timeout;
		}else{
			this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = false;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId,  BluetoothDevice device, UUID service, 
                          UUID characteristic, Handler handler, byte []value, int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_DescriptorUuid = null;
		this.m_NotifyHandler = handler;
		this.m_Value = value;
		if(timeout > 0){
		this.m_TimeOut = timeout;
		}else{
		this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = false;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
	public BtSmartRequest(RequestType type, int requestId,  BluetoothDevice device, UUID service, 
			              UUID characteristic, UUID descriptor, Handler handler, byte []value, 
			              int timeout) {
		this.m_Type = type;
		this.m_RequestId = requestId;
		this.m_SmartDevice = device;
		this.m_ServiceUuid = service;
		this.m_CharacteristicUuid = characteristic;
		this.m_DescriptorUuid = descriptor;
		this.m_NotifyHandler = handler;
		this.m_Value = value;
		if(timeout > 0){
			this.m_TimeOut = timeout;
		}else{
			this.m_TimeOut = BtSmartCommon.MESSAGE_TIME_OUT;
		}
		this.m_StartTime = System.currentTimeMillis();
		this.m_IsSynRequest = false;
		this.m_ResultValue = null;
		this.m_Rssi = 0;
		this.m_ErrCode = 0;
		this.m_IsFinish = false;
	}
	
}

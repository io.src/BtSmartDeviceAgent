package com.scy.btsmart.bluetooth;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.scy.btsmart.conf.BtProfileConfManage;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BtSmartScanService extends Service{

	private static final String TAG = "BtSmartScanService";
	
	private final IBinder mBinder = new LocalBinder();
	private BluetoothAdapter mBtAdapter = null;
	private boolean mIsScan = false;
	
	public class LocalBinder extends Binder {
		public BtSmartScanService getService() {
			// Return this instance of BtSmartService so clients can call public
			// methods.
			return BtSmartScanService.this;
		}
	}
	
	@Override
	public void onCreate() {
		//获取蓝牙设备
        mBtAdapter = ((BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        if(mBtAdapter == null){
        	return;
        }
     
        // Register for broadcasts on BluetoothAdapter state change so that we can tell if it has been turned off.
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.registerReceiver(mReceiver, filter);
        
        //检查蓝牙设备
        if(checkEnableBlooth()){
        	startScanLeDevice();
        }
	}
	
	@SuppressWarnings("deprecation")
	@Override 
    public void onStart(Intent intent, int startId) { 
            super.onStart(intent, startId); 
    }
    
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		unregisterReceiver(mReceiver);
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}
	
	//开始扫描设备
	public void startScanLeDevice(){
		if(mIsScan){
			stopScanLeDevice();
		}
		
		mBtAdapter.startLeScan(mLeScanCallback);
		mIsScan = true;
	}
	
	//停止扫描设备
	public void stopScanLeDevice(){
		if(mIsScan){
			mBtAdapter.stopLeScan(mLeScanCallback);
			mIsScan = false;
		}
	}
	
	 private boolean checkEnableBlooth(){
	     if (mBtAdapter == null || !mBtAdapter.isEnabled()) {
	    	 if(mBtAdapter == null){
	    		Log.i(TAG, "Bluetooth not exist or bad."); 
	    		return false;
	    	 }else{
	    	    mBtAdapter.enable();
	    	 }
	     }
	     
	     return true;
	 }
	 
	 /**
	 * Callback for scan results.
	 */
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback(){

	        @Override
	        public void onLeScan(final BluetoothDevice device, int rssi, byte []scanRecord) {
	        	
	        	List<UUID> scannedUUIDs = uuidsFromAdvert(scanRecord);
	        	for(int i = 0;i < scannedUUIDs.size(); i++){
	        		if(BtProfileConfManage.isUUIDSupport(scannedUUIDs.get(i))){
	        			
	        			//判断属性是支持
	    	        	BtSmartDevice tmp = new BtSmartDevice(device.getName(), device.getAddress(), rssi, scannedUUIDs);
	    	        	BtSmartDeviceManage.addSmartDevice(tmp, true);
	        			break;
	        		}
	        	}
	        }
	 };
	    
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_OFF) {
                    Toast.makeText(context, "Bluetooth disabled.", Toast.LENGTH_LONG).show();    
                    Log.i(TAG, "Bluetooth disabled."); 
                    if(mBtAdapter == null){
                    	mBtAdapter = ((BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
                    }
                    
                    mBtAdapter.enable();
                }
                else if (state == BluetoothAdapter.STATE_ON) {
                    Toast.makeText(context, "Bluetooth enabled.", Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Bluetooth disabled."); 
                    mBtAdapter = ((BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
                }
            }
        }
    };
    
    //uuid转换
    private List<UUID> uuidsFromAdvert(final byte[] advData) {
        final List<UUID> uuids = new ArrayList<UUID>();

        // Pointer within advData to the start of the current ad element being processed.
        int ptrToAdElement = 0;

        // Offsets from start of ad element (i.e. from ptrToAdElement).
        final int OFFSET_LENGTH = 0;
        final int OFFSET_TYPE = 1;
        final int OFFSET_DATA = 2;

        final byte AD_TYPE_UUID_16BIT = 0x02;
        final byte AD_TYPE_UUID_16BIT_LIST = 0x03;
        final byte AD_TYPE_UUID_128BIT = 0x06;
        final byte AD_TYPE_UUID_128BIT_LIST = 0x07;

        final int UUID_16_LENGTH = 2;
        final int UUID_128_LENGTH = 16;

        final String BASE_UUID_FORMAT = "%08x-0000-1000-8000-00805f9b34fb";

        while (ptrToAdElement < advData.length - 1) {
            final byte length = advData[ptrToAdElement + OFFSET_LENGTH];

            // The advert data returned by the Android API is padded out with trailing zeroes, so if we reach a
            // zero length then we are done.
            if (length == 0)
                break;

            // Check that there is enough remaining data in the advert for the indicated length.
            if (length > (advData.length - ptrToAdElement - 1)) {
                // This was a malformed advert so return an empty list, even if we got some UUIDs already.
                uuids.clear();
                return uuids;
            }

            final byte adType = advData[ptrToAdElement + OFFSET_TYPE];

            switch (adType) {
            case AD_TYPE_UUID_16BIT:
            case AD_TYPE_UUID_16BIT_LIST:
                for (int i = length; i > UUID_16_LENGTH - 1; i -= UUID_16_LENGTH) {
                    int uuid16 = (advData[ptrToAdElement + OFFSET_DATA] & 0xFF);
                    uuid16 |= ((advData[ptrToAdElement + OFFSET_DATA + 1] & 0xFF) << 8);
                    uuids.add(UUID.fromString(String.format(BASE_UUID_FORMAT, uuid16)));
                }
                break;
            case AD_TYPE_UUID_128BIT:
            case AD_TYPE_UUID_128BIT_LIST:
                for (int i = length; i > UUID_128_LENGTH - 1; i -= UUID_128_LENGTH) {
                    long msb = ((advData[ptrToAdElement + OFFSET_DATA] & 0xFF) << 56);
                    msb |= ((advData[ptrToAdElement + OFFSET_DATA + 1] & 0xFF) << 48);
                    msb |= ((advData[ptrToAdElement + OFFSET_DATA + 2] & 0xFF) << 40);
                    msb |= ((advData[ptrToAdElement + OFFSET_DATA + 3] & 0xFF) << 32);
                    msb |= ((advData[ptrToAdElement + OFFSET_DATA + 4] & 0xFF) << 24);
                    msb |= ((advData[ptrToAdElement + OFFSET_DATA + 5] & 0xFF) << 16);
                    msb |= ((advData[ptrToAdElement + OFFSET_DATA + 6] & 0xFF) << 8);
                    msb |= ((advData[ptrToAdElement + OFFSET_DATA + 7] & 0xFF) << 0);
                    long lsb = ((advData[ptrToAdElement + OFFSET_DATA + 8] & 0xFF) << 56);
                    lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 9] & 0xFF) << 48);
                    lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 10] & 0xFF) << 40);
                    lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 11] & 0xFF) << 32);
                    lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 12] & 0xFF) << 24);
                    lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 13] & 0xFF) << 16);
                    lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 14] & 0xFF) << 8);
                    lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 15] & 0xFF) << 0);
                    uuids.add(new UUID(msb, lsb));
                }
                break;
            default:
                // An advert type we don't care about.
                break;
            }

            // Length byte isn't included in length, hence the +1.
            ptrToAdElement += length + 1;
        }

        return uuids;
    }
}

package com.scy.btsmart.bluetooth;

import java.lang.ref.WeakReference;

import com.scy.btsmart.common.BtSmartCommon;

import android.os.Handler;
import android.os.Message;


public class BtSmartDefaultNotifyHandler extends Handler {
	
	private final WeakReference<BtSmartMessageService> mSmartService;
	
	public BtSmartDefaultNotifyHandler(BtSmartMessageService service){
		mSmartService = new WeakReference<BtSmartMessageService>(service);
	}
	
    @Override
    public void handleMessage(Message msg) {
    	BtSmartMessageService parentservice = mSmartService.get();
        if (parentservice != null) {
            switch (msg.what) {                
            case BtSmartCommon.MESSAGE_CONNECTED: {
                // Cancel the connect timer.
            		//parentservice.SetConnectStatus(true);
                }
                break;
            case BtSmartCommon.MESSAGE_DISCONNECTED: {
                // End this activity and go back to scan results view.
            		//parentservice.SetConnectStatus(false);
                break;
            }
          }
        }
    }
}

/*
 * Copyright: (C)2009 VASDAQ Japan,ltd. All Rights Reserved. 
 * 
 * Author: scy Opensource Software Inc.
 *
 * License: GPL2.0
 *
 */
package com.scy.btsmart.bluetooth;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class BtSmartDevice {

	public String                         mDeviceName;  
	public String                         mDeviceAddress;
	public int                            mRssi;
	public List<UUID>                     mSUUIDList;
	
	public BtSmartDevice(String name, String address, int rssi, List<UUID> uuidlist){
    	this.mDeviceName = name;
    	this.mDeviceAddress = address;
    	this.mRssi = rssi;
    	
    	mSUUIDList = new ArrayList<UUID>();  
    	for(UUID uuid : uuidlist){ 
    		mSUUIDList.add(uuid);
        }
	}
}

package com.scy.btsmart.conf;

public class ConfParamName {
	
	public static final String  HTTP_PORT = "HttpPort";
	
	public static final String  MULTICAST_IP = "MulticastIP";
	
	public static final String  MULTICAST_PORT = "MulticastPort";
}

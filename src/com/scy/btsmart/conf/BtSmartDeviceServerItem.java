package com.scy.btsmart.conf;

import java.util.ArrayList;
import java.util.UUID;


public class BtSmartDeviceServerItem {
	public String                       m_ServerName;
	public UUID                         m_ServerUUID; 
	public String                       m_Des;
	public ArrayList<BtCharacteristic>  m_CharacteristicList;
	
	public BtSmartDeviceServerItem(String name, String uuid, String des, boolean isinitlist){
		this.m_ServerName = name;
		this.m_ServerUUID = UUID.fromString(uuid);
		this.m_Des = des;
		if(isinitlist){
			this.m_CharacteristicList = new ArrayList<BtCharacteristic>();
		}
	}
	
	public class BtCharacteristic{
		public String  m_Name;
		public UUID    m_UUID;
		public String  m_Des;
		
		public BtCharacteristic(String name, String uuid, String des){
			this.m_Name = name;
			this.m_UUID = UUID.fromString(uuid);
			this.m_Des = des;
		}
	}
}

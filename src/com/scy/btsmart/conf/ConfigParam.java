package com.scy.btsmart.conf;

public class ConfigParam {
	
	//����
	public static final String VALUE_TYPE_INT    = "int";
	
	//�ַ�����
	public static final String VALUE_TYPE_STRING = "string";
	
    private String    m_Key;
    private String    m_Value;
    private int       m_IntValue;
    private String    m_Type;
 
    public ConfigParam(String key, String value, String type)
    {
    	if(key == null || value == null || type == null){
    		return;
    	}
    	
    	this.m_Key = key;
    	if(type.equals(VALUE_TYPE_INT)){
    		this.m_IntValue = Integer.parseInt(value);
    	}else{
    		this.m_Value = value;
    	}	
    	
    	this.m_Type = type;
    }
	public String getM_Key() {
		return m_Key;
	}
	public void setM_Key(String m_Key) {
		this.m_Key = m_Key;
	}
	public String getM_Value() {
		return m_Value;
	}
	public void setM_Value(String m_Value) {
		this.m_Value = m_Value;
	}
	public String getM_Type() {
		return m_Type;
	}
	public void setM_Type(String m_Type) {
		this.m_Type = m_Type;
	}

	public int getM_IntValue() {
		return m_IntValue;
	}

	public void setM_IntValue(int m_IntValue) {
		this.m_IntValue = m_IntValue;
	}
}

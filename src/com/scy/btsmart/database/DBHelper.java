package com.scy.btsmart.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	private static final String TAG = "DBHelper";
	
	//数据库名称
	private static final String DB_NAME = "BtSmartDevice.db";
	
	//数据库版本
	private static final int DB_VERSION = 1;
	
	//表名
	public static final String BTSMART_DEVICE_TABLE = "btsmart_device_table";
	
	//设备名
	public static final String  COL_NAME = "name";
	
	//设备mac
	public static final String  COL_MAC = "mac";
	
	//设备服务uuid
	public static final String  COL_UUID = "uuid";
	
	public DBHelper(Context context) {
		this(context, DB_NAME, null, DB_VERSION);
	}

	public DBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE IF NOT EXISTS "
				    + BTSMART_DEVICE_TABLE
				    + "(id integer primary key autoincrement, COL_NAME"+ " text," + COL_MAC + " text," 
				    +  COL_UUID + " text)";
		Log.i(TAG, "onCreate sql--->" + sql); 
		db.execSQL(sql);  
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		  db.execSQL("drop table if exists "+BTSMART_DEVICE_TABLE);
          onCreate(db);
	}

}
